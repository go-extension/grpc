package credentials

import (
	"context"
	"crypto/x509"
	"fmt"
	"net"
	"net/url"
	"os"

	"gitlab.com/go-extension/tls"

	credinternal "gitlab.com/go-extension/grpc/internal/credentials"
	"google.golang.org/grpc/credentials"
)

// TLSInfo contains the auth information for a TLS authenticated connection.
// It implements the AuthInfo interface.
type TLSInfo struct {
	State tls.ConnectionState
	credentials.CommonAuthInfo
	// This API is experimental.
	SPIFFEID *url.URL
}

// AuthType returns the type of TLSInfo as a string.
func (t TLSInfo) AuthType() string {
	return "tls"
}

// GetSecurityValue returns security info requested by channelz.
func (t TLSInfo) GetSecurityValue() credentials.ChannelzSecurityValue {
	v := &TLSChannelzSecurityValue{
		StandardName: cipherSuiteLookup[t.State.CipherSuite],
	}
	// Currently there's no way to get LocalCertificate info from tls package.
	if len(t.State.PeerCertificates) > 0 {
		v.RemoteCertificate = t.State.PeerCertificates[0].Raw
	}
	return v
}

// tlsCreds is the credentials required for authenticating a connection using TLS.
type tlsCreds struct {
	// TLS configuration
	config *tls.Config
}

func (c tlsCreds) Info() credentials.ProtocolInfo {
	return credentials.ProtocolInfo{
		SecurityProtocol: "tls",
		SecurityVersion:  "1.2",
		ServerName:       c.config.ServerName,
	}
}

func (c *tlsCreds) ClientHandshake(ctx context.Context, authority string, rawConn net.Conn) (_ net.Conn, _ credentials.AuthInfo, err error) {
	// use local cfg to avoid clobbering ServerName if using multiple endpoints
	cfg := credinternal.CloneTLSConfig(c.config)
	if cfg.ServerName == "" {
		serverName, _, err := net.SplitHostPort(authority)
		if err != nil {
			// If the authority had no host port or if the authority cannot be parsed, use it as-is.
			serverName = authority
		}
		cfg.ServerName = serverName
	}
	conn := tls.Client(rawConn, cfg)
	errChannel := make(chan error, 1)
	go func() {
		errChannel <- conn.Handshake()
		close(errChannel)
	}()
	select {
	case err := <-errChannel:
		if err != nil {
			conn.Close()
			return nil, nil, err
		}
	case <-ctx.Done():
		conn.Close()
		return nil, nil, ctx.Err()
	}
	tlsInfo := TLSInfo{
		State: conn.ConnectionState(),
		CommonAuthInfo: credentials.CommonAuthInfo{
			SecurityLevel: credentials.PrivacyAndIntegrity,
		},
	}
	id := credinternal.SPIFFEIDFromState(conn.ConnectionState())
	if id != nil {
		tlsInfo.SPIFFEID = id
	}
	return credinternal.WrapSyscallConn(rawConn, conn), tlsInfo, nil
}

func (c *tlsCreds) ServerHandshake(rawConn net.Conn) (net.Conn, credentials.AuthInfo, error) {
	conn := tls.Server(rawConn, c.config)
	if err := conn.Handshake(); err != nil {
		conn.Close()
		return nil, nil, err
	}
	tlsInfo := TLSInfo{
		State: conn.ConnectionState(),
		CommonAuthInfo: credentials.CommonAuthInfo{
			SecurityLevel: credentials.PrivacyAndIntegrity,
		},
	}
	id := credinternal.SPIFFEIDFromState(conn.ConnectionState())
	if id != nil {
		tlsInfo.SPIFFEID = id
	}
	return credinternal.WrapSyscallConn(rawConn, conn), tlsInfo, nil
}

func (c *tlsCreds) Clone() credentials.TransportCredentials {
	return NewTLS(c.config)
}

func (c *tlsCreds) OverrideServerName(serverNameOverride string) error {
	c.config.ServerName = serverNameOverride
	return nil
}

// NewTLS uses c to construct a TransportCredentials based on TLS.
func NewTLS(c *tls.Config) credentials.TransportCredentials {
	tc := &tlsCreds{credinternal.CloneTLSConfig(c)}
	tc.config.NextProtos = credinternal.AppendH2ToNextProtos(tc.config.NextProtos)
	return tc
}

// NewClientTLSFromCert constructs TLS credentials from the provided root
// certificate authority certificate(s) to validate server connections. If
// certificates to establish the identity of the client need to be included in
// the credentials (eg: for mTLS), use NewTLS instead, where a complete
// tls.Config can be specified.
// serverNameOverride is for testing only. If set to a non empty string,
// it will override the virtual host name of authority (e.g. :authority header
// field) in requests.
func NewClientTLSFromCert(cp *x509.CertPool, serverNameOverride string) credentials.TransportCredentials {
	return NewTLS(&tls.Config{ServerName: serverNameOverride, RootCAs: cp})
}

// NewClientTLSFromFile constructs TLS credentials from the provided root
// certificate authority certificate file(s) to validate server connections. If
// certificates to establish the identity of the client need to be included in
// the credentials (eg: for mTLS), use NewTLS instead, where a complete
// tls.Config can be specified.
// serverNameOverride is for testing only. If set to a non empty string,
// it will override the virtual host name of authority (e.g. :authority header
// field) in requests.
func NewClientTLSFromFile(certFile, serverNameOverride string) (credentials.TransportCredentials, error) {
	b, err := os.ReadFile(certFile)
	if err != nil {
		return nil, err
	}
	cp := x509.NewCertPool()
	if !cp.AppendCertsFromPEM(b) {
		return nil, fmt.Errorf("credentials: failed to append certificates")
	}
	return NewTLS(&tls.Config{ServerName: serverNameOverride, RootCAs: cp}), nil
}

// NewServerTLSFromCert constructs TLS credentials from the input certificate for server.
func NewServerTLSFromCert(cert *tls.Certificate) credentials.TransportCredentials {
	return NewTLS(&tls.Config{Certificates: []tls.Certificate{*cert}})
}

// NewServerTLSFromFile constructs TLS credentials from the input certificate file and key
// file for server.
func NewServerTLSFromFile(certFile, keyFile string) (credentials.TransportCredentials, error) {
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, err
	}
	return NewTLS(&tls.Config{Certificates: []tls.Certificate{cert}}), nil
}

// TLSChannelzSecurityValue defines the struct that TLS protocol should return
// from GetSecurityValue(), containing security info like cipher and certificate used.
//
// # Experimental
//
// Notice: This type is EXPERIMENTAL and may be changed or removed in a
// later release.
type TLSChannelzSecurityValue struct {
	credentials.ChannelzSecurityValue
	StandardName      string
	LocalCertificate  []byte
	RemoteCertificate []byte
}

var cipherSuiteLookup = map[uint16]string{
	tls.TLS_RSA_WITH_RC4_128_SHA:                      "TLS_RSA_WITH_RC4_128_SHA",
	tls.TLS_RSA_WITH_3DES_EDE_CBC_SHA:                 "TLS_RSA_WITH_3DES_EDE_CBC_SHA",
	tls.TLS_RSA_WITH_AES_128_CBC_SHA:                  "TLS_RSA_WITH_AES_128_CBC_SHA",
	tls.TLS_RSA_WITH_AES_256_CBC_SHA:                  "TLS_RSA_WITH_AES_256_CBC_SHA",
	tls.TLS_RSA_WITH_AES_128_CBC_SHA256:               "TLS_RSA_WITH_AES_128_CBC_SHA256",
	tls.TLS_RSA_WITH_ARIA_128_CBC_SHA256:              "TLS_RSA_WITH_ARIA_128_CBC_SHA256",
	tls.TLS_RSA_WITH_ARIA_256_CBC_SHA384:              "TLS_RSA_WITH_ARIA_256_CBC_SHA384",
	tls.TLS_RSA_WITH_AES_128_GCM_SHA256:               "TLS_RSA_WITH_AES_128_GCM_SHA256",
	tls.TLS_RSA_WITH_AES_256_GCM_SHA384:               "TLS_RSA_WITH_AES_256_GCM_SHA384",
	tls.TLS_RSA_WITH_AES_128_CCM_SHA256:               "TLS_RSA_WITH_AES_128_CCM_SHA256",
	tls.TLS_RSA_WITH_AES_256_CCM_SHA256:               "TLS_RSA_WITH_AES_256_CCM_SHA256",
	tls.TLS_RSA_WITH_AES_128_CCM_8_SHA256:             "TLS_RSA_WITH_AES_128_CCM_8_SHA256",
	tls.TLS_RSA_WITH_AES_256_CCM_8_SHA256:             "TLS_RSA_WITH_AES_256_CCM_8_SHA256",
	tls.TLS_RSA_WITH_ARIA_128_GCM_SHA256:              "TLS_RSA_WITH_ARIA_128_GCM_SHA256",
	tls.TLS_RSA_WITH_ARIA_256_GCM_SHA384:              "TLS_RSA_WITH_ARIA_256_GCM_SHA384",
	tls.TLS_RSA_WITH_CAMELLIA_128_GCM_SHA256:          "TLS_RSA_WITH_CAMELLIA_128_GCM_SHA256",
	tls.TLS_RSA_WITH_CAMELLIA_256_GCM_SHA384:          "TLS_RSA_WITH_CAMELLIA_256_GCM_SHA384",
	tls.TLS_ECDHE_ECDSA_WITH_RC4_128_SHA:              "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA",
	tls.TLS_ECDHE_RSA_WITH_RC4_128_SHA:                "TLS_ECDHE_RSA_WITH_RC4_128_SHA",
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA:          "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA",
	tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA:            "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
	tls.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA:          "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA",
	tls.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384:       "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384",
	tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA:            "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
	tls.TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA:         "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA",
	tls.TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA:           "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA",
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256:       "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256",
	tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256:         "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_ARIA_128_CBC_SHA256:      "TLS_ECDHE_ECDSA_WITH_ARIA_128_CBC_SHA256",
	tls.TLS_ECDHE_RSA_WITH_ARIA_128_CBC_SHA256:        "TLS_ECDHE_RSA_WITH_ARIA_128_CBC_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_ARIA_256_CBC_SHA384:      "TLS_ECDHE_ECDSA_WITH_ARIA_256_CBC_SHA384",
	tls.TLS_ECDHE_RSA_WITH_ARIA_256_CBC_SHA384:        "TLS_ECDHE_RSA_WITH_ARIA_256_CBC_SHA384",
	tls.TLS_ECDHE_ECDSA_WITH_CAMELLIA_128_CBC_SHA256:  "TLS_ECDHE_ECDSA_WITH_CAMELLIA_128_CBC_SHA256",
	tls.TLS_ECDHE_RSA_WITH_CAMELLIA_128_CBC_SHA256:    "TLS_ECDHE_RSA_WITH_CAMELLIA_128_CBC_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_CAMELLIA_256_CBC_SHA384:  "TLS_ECDHE_ECDSA_WITH_CAMELLIA_256_CBC_SHA384",
	tls.TLS_ECDHE_RSA_WITH_CAMELLIA_256_CBC_SHA384:    "TLS_ECDHE_RSA_WITH_CAMELLIA_256_CBC_SHA384",
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256:       "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256",
	tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256:         "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384:       "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
	tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384:         "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_CCM_SHA256:       "TLS_ECDHE_ECDSA_WITH_AES_128_CCM_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_AES_256_CCM_SHA256:       "TLS_ECDHE_ECDSA_WITH_AES_256_CCM_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8_SHA256:     "TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8_SHA256:     "TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256: "TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256",
	tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256:   "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_ARIA_128_GCM_SHA256:      "TLS_ECDHE_ECDSA_WITH_ARIA_128_GCM_SHA256",
	tls.TLS_ECDHE_RSA_WITH_ARIA_128_GCM_SHA256:        "TLS_ECDHE_RSA_WITH_ARIA_128_GCM_SHA256",
	tls.TLS_ECDHE_ECDSA_WITH_ARIA_256_GCM_SHA384:      "TLS_ECDHE_ECDSA_WITH_ARIA_256_GCM_SHA384",
	tls.TLS_ECDHE_RSA_WITH_ARIA_256_GCM_SHA384:        "TLS_ECDHE_RSA_WITH_ARIA_256_GCM_SHA384",
	tls.TLS_ECDHE_ECDSA_WITH_CAMELLIA_128_GCM_SHA256:  "TLS_ECDHE_ECDSA_WITH_CAMELLIA_128_GCM_SHA256",
	tls.TLS_ECDHE_RSA_WITH_CAMELLIA_128_GCM_SHA256:    "TLS_ECDHE_RSA_WITH_CAMELLIA_128_GCM_SHA256",
	tls.TLS_ECDHE_RSA_WITH_CAMELLIA_256_GCM_SHA384:    "TLS_ECDHE_RSA_WITH_CAMELLIA_256_GCM_SHA384",
	tls.TLS_ECDHE_ECDSA_WITH_CAMELLIA_256_GCM_SHA384:  "TLS_ECDHE_ECDSA_WITH_CAMELLIA_256_GCM_SHA384",
	tls.TLS_AES_128_GCM_SHA256:                        "TLS_AES_128_GCM_SHA256",
	tls.TLS_AES_256_GCM_SHA384:                        "TLS_AES_256_GCM_SHA384",
	tls.TLS_CHACHA20_POLY1305_SHA256:                  "TLS_CHACHA20_POLY1305_SHA256",
	tls.TLS_AES_128_CCM_SHA256:                        "TLS_AES_128_CCM_SHA256",
	tls.TLS_AES_128_CCM_8_SHA256:                      "TLS_AES_128_CCM_8_SHA256",
	tls.TLS_FALLBACK_SCSV:                             "TLS_FALLBACK_SCSV",
}

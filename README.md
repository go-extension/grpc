# Google Remote Procedure Call (gRPC)

This repository is an extension of the google grpc library [google.golang.org/grpc](https://google.golang.org/grpc)

# Features

- [Extension TLS](gitlab.com/go-extension/tls)
